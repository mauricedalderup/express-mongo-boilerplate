import mongoose from "mongoose";
import util from "util";
import bluebird from "bluebird";

import https from "https";
import fs from "fs";

// config should be imported before importing any other file
import config from "../config/config";

// Other imports
import app from "../config/express";
import Logger from "../config/Log";

const debug = require("debug")("express-mongoose-es6-rest-api:index");

// make bluebird default Promise
Promise = bluebird; // eslint-disable-line no-global-assign

if (config.useDb) {
  // plugin bluebird promise in mongoose
  mongoose.Promise = Promise;

  // connect to mongo db
  const mongoUri = config.mongo;
  mongoose.connect(
    mongoUri,
    {
      useNewUrlParser: true,
      poolSize: 2,
      promiseLibrary: global.Promise
    }
  );
  mongoose.connection.on("error", () => {
    throw new Error(`unable to connect to database: ${mongoUri}`);
  });

  // print mongoose logs in dev env
  if (config.mongooseDebug) {
    mongoose.set("debug", (collectionName, method, query, doc) => {
      debug(`${collectionName}.${method}`, util.inspect(query, false, 20), doc);
    });
  }
}

// module.parent check is required to support mocha watch
// src: https://github.com/mochajs/mocha/issues/1912
if (!module.parent) {
  app.listen(config.port, () => {
    Logger().info(`server started on port ${config.port} (${config.env})`);
  });

  if (config.env === "PRODUCTION") {
    const options = {
      key: fs.readFileSync(
        "/etc/letsencrypt/live/test-vlaio.bewire.services/fullchain.pem"
      ),
      cert: fs.readFileSync(
        "/etc/letsencrypt/live/test-vlaio.bewire.services/privkey.pem"
      )
    };
    https.createServer(options, app).listen(8080);
  }
}

export default app;
