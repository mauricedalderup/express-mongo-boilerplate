import { Router } from "express";

import authRoutes from "./endpoints/auth/auth.route";

export default Router()
  .get("/health-check", (req, res) =>
    res.send("OK")
  ) /** GET /health-check - Check service health */

  .use("/auth", authRoutes); // mount movement routes at /movement
