import Joi from "joi";

// require and configure dotenv, will load vars in .env in PROCESS.ENV
import dotenv from "dotenv";

dotenv.config();

// define validation for all the env vars
const envVarsSchema = Joi.object({
  NODE_ENV: Joi.string()
    .allow(["development", "production", "test", "provision"])
    .default("development"),
  PORT: Joi.number().default(3000),
  MONGOOSE_DEBUG: Joi.boolean().when("NODE_ENV", {
    is: Joi.string().equal("development"),
    then: Joi.boolean().default(true),
    otherwise: Joi.boolean().default(false)
  }),
  JWT_SECRET: Joi.string()
    .required()
    .description("JWT Secret required to sign"),
  MONGO_URI: Joi.string()
    .required()
    .description("Mongo DB host url"),
  MONGO_HOST_TEST: Joi.string()
    .required()
    .description("Mongo DB test host url"),
  MONGO_PORT: Joi.number().default(27017),
  USE_DB: Joi.boolean().default(false),
  SALT_ROUNDS: Joi.number().required()
})
  .unknown()
  .required();

const JoiValidate = Joi.validate(process.env, envVarsSchema);
const { error } = JoiValidate;
const envVars = JoiValidate.value;

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

const config = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  mongooseDebug: envVars.MONGOOSE_DEBUG,
  jwtSecret: envVars.JWT_SECRET,
  mongo: envVars.MONGO_URI,
  mongo_test: envVars.MONGO_HOST_TEST,
  useDb: envVars.USE_DB,
  saltRounds: envVars.SALT_ROUNDS
};

export default config;
